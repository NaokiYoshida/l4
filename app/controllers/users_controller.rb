class UsersController < ApplicationController
  def main
    if session[:login_uid] != nil
      render 'login'
    elsif session[:login_uid] == nil
      render 'main'
    end
  end

  def login
    # p session[:login_uid]

    if User.authenticate(params[:uid], params[:pass])
      session[:login_uid] = params[:uid]
      redirect_to users_main_path
    else
      render 'error'
    end
  end

  def error

  end

  def logout

    session.delete(:login_uid)
    redirect_to users_main_path
  end
end
