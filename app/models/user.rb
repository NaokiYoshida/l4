class User < ApplicationRecord
  validates :uid , uniqueness: true
  def self.authenticate(uid, pass)
    if user = find_by(uid: uid)
      if BCrypt::Password.new(user.pass) == pass
        user
      end
    else
      nil
    end
  end
end
